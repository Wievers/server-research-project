import os, sys
import pickle as pc

FName = sys.argv
os.chdir(os.path.dirname(FName[1]))
folder_name = os.path.basename(FName[1])

filesName = dict()
for (root,dirs,files) in os.walk(folder_name, topdown=True):
    for i in files:
        filesName[os.path.join(root, i)] = {"Name" : i, "Path" : root, "Size" : os.stat(os.path.join(root, i)).st_size, "Latest" : os.stat(os.path.join(root, i)).st_mtime, "IsFile" : True}
    for i in dirs:
        filesName[os.path.join(root, i)] = {"Name" : i, "Path" : root, "Size" : os.stat(os.path.join(root, i)).st_size, "Latest" : os.stat(os.path.join(root, i)).st_mtime, "IsFile" : False}


with open(os.path.join(folder_name, 'dirMapp.pickle'), 'wb') as handle:
    pc.dump(filesName, handle)
    pc.dump(None, handle)
