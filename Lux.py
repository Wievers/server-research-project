import sys, time
import datetime as dt
import os.path as pth

import pyperclip as clip #pip3 needed
from PySide2 import QtWidgets as qtw
from PySide2.QtCore import Qt, Slot, QTextStream, QIODevice, QFile, QEvent
from PySide2 import QtGui as qtg

import srplib2 as srp
import SrpConfigInterface as srpUi

class AdvTrees(qtw.QTreeWidget):
    """
        - name [list] : List of the headers of the Qt Tree Widget"""
    def __init__(self, name, parent = None, remote = False):
        super(AdvTrees, self).__init__(parent)
        self.setHeaderLabels(name)
        self.setContextMenuPolicy(Qt.CustomContextMenu)
        self.customContextMenuRequested.connect(self.contextMenu)
        self.parent = parent
        self.popMenu = qtw.QMenu(self)
        self.remote = remote
        self.popMenuActions = [None, None, None]
        if not self.remote:
            self.popMenuActions[0] = qtw.QAction('Send', self)
            self.popMenuActions[0].triggered.connect(self.send)
        else:
            self.popMenuActions[0] = qtw.QAction('Get', self)
            self.popMenuActions[0].triggered.connect(self.get)
        self.popMenuActions[1] = qtw.QAction('Copy full path', self)
        self.popMenuActions[1].triggered.connect(self.cfp)
        self.popMenuActions[2] = qtw.QAction('Delete', self)
        self.popMenuActions[2].triggered.connect(self.deleteF)
        for i in self.popMenuActions:
            self.popMenu.addAction(i)

        #self.popMenu.addSeparator()

    def addItems(self, elements):
        """Method to put items in a tree qt widget using a folderview:
            - elements [folderview] : The folderview used to model a directory"""
        NewDtParents = {}
        for x in elements.getTree():
            for i in x:
                bufferTime = dt.datetime.fromtimestamp(i["Latest"])
                size = "{} B".format(str(i["Size"])) if float(i["Size"]) < 800 else "{} KiB".format(str(i["Size"]/1000))
                if i["IsFile"]:
                    NewDtChild = qtw.QTreeWidgetItem(NewDtParents[pth.basename(i["Path"])], [i["Name"], size, "{}/{}/{}".format(bufferTime.day, bufferTime.month, bufferTime.year), str(i["Id"])])
                else:
                    if i["Path"] == "":
                        NewDtParents[i["Name"]] = qtw.QTreeWidgetItem(self, [i["Name"], "", "", str(i["Id"])])
                        self.expandItem(NewDtParents[i["Name"]])
                    else: NewDtParents[i["Name"]] = qtw.QTreeWidgetItem(NewDtParents[pth.basename(i["Path"])], [i["Name"], size, "{}/{}/{}".format(bufferTime.day, bufferTime.month, bufferTime.year), str(i["Id"])])

    def reload(self, element):
        """Method that reload a folderview in the qt Tree Widget
            - element [folderview] : The folderview that we want to reload"""
        self.clear()
        self.addItems(element)

    def contextMenu(self, point):
        # show context menu
        self.popMenu.exec_(self.mapToGlobal(point))

    def selectedItemsId(self):
        namesId = []
        for i in self.selectedItems():
            namesId.append(int(i.text(3)))
        return namesId

    @Slot()
    def send(self):
        self.parent.parent.send(self.selectedItemsId())

    @Slot()
    def get(self):
        self.parent.parent.retrieve(self.selectedItemsId())

    @Slot()
    def deleteF(self):
        self.parent.parent.deleteFiles(self.selectedItemsId(), self.remote)

    @Slot()
    def cfp(self):
        self.parent.parent.cfp(self.selectedItemsId(), self.remote)

class LuxMainWidget(qtw.QWidget):
    def __init__(self, parent = None):
        super(LuxMainWidget, self).__init__(parent)
        self.locNav = AdvTrees(["Local Files", "Size", "Last"], self)
        self.remNav = AdvTrees(["Remote Files", "Size", "Last"], self, True)
        self.centerWidget = qtw.QLabel()
        self.parent = parent

        self.initUi()

    def initUi(self):

        VSplitNav = qtw.QSplitter(Qt.Horizontal)
        VSplitNav.addWidget(self.locNav)
        VSplitNav.addWidget(self.remNav)
        VSplitNav.setCollapsible(0, False)
        VSplitNav.setCollapsible(1, False)

        """HSplitNav = qtw.QSplitter(Qt.Horizontal)
        HSplitNav.addWidget(VSplitNav)
        HSplitNav.addWidget(self.centerWidget)
        HSplitNav.setStretchFactor(1, 1)
        HSplitNav.setSizes([260, 150])"""

        layout = qtw.QHBoxLayout()
        layout.setContentsMargins(0,5,0,0)
        layout.addWidget(VSplitNav)
        self.setLayout(layout)


class Lux(qtw.QMainWindow):
    def __init__(self):
        super(Lux, self).__init__()
        # Parameters of the window
        self.setWindowTitle("Lux V5.1")
        self.setWindowIcon(qtg.QIcon("Graphics/logo-srp.png"))
        self.width = 700
        self.height = int(0.618 * self.width)
        self.resize(self.width, self.height)

        # Attributes of the class
        self.creds = [None, None]
        self.srpSystem = None #Controller => SRP

        # Widgets of the program
        self.mainWidget = LuxMainWidget(self)

        # Widget For Main Window
        self.menu = self.menuBar()
        self.status = self.statusBar()
        self.fileMenu = [self.menu.addMenu("File"), self.menu.addMenu("Transfer"), self.menu.addMenu("Parameters")]
        self.actTransfer = [None for i in range(7)] #We need to access this action easily


        self.initUi()

    def initUi(self):
        self.status.addWidget(qtw.QLabel(str(dt.datetime.today())))

        actFile = [None for i in range(5)]
        actFile[0] = qtw.QAction("New Window (WIP)", self)
        actFile[0].setShortcut("Ctrl+N")
        #actFile[0].triggered.connect(self.get_login)
        actFile[1] = qtw.QAction("Open File", self)
        actFile[1].triggered.connect(self.openFile)
        actFile[2] = qtw.QAction("Open Folder", self)
        actFile[2].triggered.connect(self.openFolder)
        actFile[3] = qtw.QAction("Save", self)
        actFile[3].setShortcut("Ctrl+S")
        actFile[3].triggered.connect(self.save)
        actFile[4] = qtw.QAction("Quit", self)
        actFile[4].setShortcut("Ctrl+X")
        actFile[4].triggered.connect(self.exit)
        for i in range(len(actFile)):
            self.fileMenu[0].addAction(actFile[i])


        self.actTransfer[0] = qtw.QAction("Connect", self)
        self.actTransfer[0].triggered.connect(self.connect)
        self.actTransfer[1] = qtw.QAction("Disconnect", self)
        self.actTransfer[1].triggered.connect(self.disconnect)
        self.actTransfer[1].setEnabled(False)
        self.actTransfer[2] = qtw.QAction("Refresh", self)
        self.actTransfer[2].triggered.connect(self.refresh)
        self.actTransfer[3] = qtw.QAction("Server Status", self)
        self.actTransfer[3].triggered.connect(self.synchronize)
        self.actTransfer[4] = qtw.QAction("Send All", self)
        self.actTransfer[4].triggered.connect(self.send)
        self.actTransfer[5] = qtw.QAction("Retrieve All", self)
        self.actTransfer[5].triggered.connect(self.retrieve)
        self.actTransfer[6] = qtw.QAction("Transfer Logs", self)
        for j in self.actTransfer:
            self.fileMenu[1].addAction(j)

        actParam = [None for i in range(2)]
        actParam[0] = qtw.QAction("SRP Parameters", self)
        actParam[0].triggered.connect(self.srpParam)
        actParam[1] = qtw.QAction("SRP Uninstall", self)
        actParam[1].triggered.connect(self.srpUninstall)
        for j in actParam:
            self.fileMenu[2].addAction(j)

        self.setCentralWidget(self.mainWidget)

    @Slot()
    def openFile(self):
        files = qtw.QFileDialog()
        files.exec_()
        print(files.selectedFiles())
        #self.cont.addFileLocal()

    @Slot()
    def openFolder(self):
        files = qtw.QFileDialog()
        files.setOption(qtw.QFileDialog.ShowDirsOnly, True)
        files.setFileMode(qtw.QFileDialog.DirectoryOnly)
        files.exec_()
        self.srpSystem.addFileLocal(files.directory().absolutePath())
        self.mainWidget.locNav.clear()
        self.mainWidget.locNav.addItems(self.srpSystem.localCompFiles)

    @Slot()
    def save(self):
        pass

    @Slot()
    def exit(self):
        qtw.QApplication.quit()

    @Slot()
    def connect(self):
        login = srpUi.IdentificationForm()
        if not login.exec_():
            self.creds[0] = login.username.text()
            self.creds[1] = login.password.text()
        try:
            self.srpSystem = srp.ServInteract(self.creds[0], self.creds[1])
            self.srpSystem.allFilesList()
            self.mainWidget.locNav.addItems(self.srpSystem.localCompFiles)
            self.mainWidget.remNav.addItems(self.srpSystem.remoteCompFiles)
            self.actTransfer[1].setEnabled(True)
            self.actTransfer[0].setEnabled(False)

        except srp.exceptionSSH as e:
            srpUi.PopupBox(info_text = str(e), title = "Error SRP", modal = True)

    @Slot()
    def disconnect(self):
        self.srpSystem = None
        self.mainWidget.locNav.clear()
        self.mainWidget.remNav.clear()
        self.actTransfer[1].setEnabled(False)
        self.actTransfer[0].setEnabled(True)
        srpUi.PopupBox(info_text = "Successfully disconnected from the remote server", title = "Information SRP", modal = True, icon = qtw.QMessageBox.Information)

    @Slot()
    def refresh(self):
        self.srpSystem.allFilesList()
        self.mainWidget.locNav.reload(self.srpSystem.localCompFiles)
        self.mainWidget.remNav.reload(self.srpSystem.remoteCompFiles)

    @Slot()
    def synchronize(self):
        pass

    @Slot()
    def send(self, namesFiles = None):
        loadingPop = srpUi.LoadingForm(self, "Upload", "Sending files...")

        try:
            self.srpSystem.fileSaverSRP(lambda a, b : loadingPop.actionProgress(a, b), namesFiles) #Sending the files AND keeping track of the progress
            srpUi.PopupBox(info_text = "Files successfully sent!", icon = qtw.QMessageBox.Information, modal = False)
            self.mainWidget.locNav.reload(self.srpSystem.localCompFiles)
            self.mainWidget.remNav.reload(self.srpSystem.remoteCompFiles)

        except srp.exceptionSSH as e:
            loadingPop.close()
            srpUi.PopupBox(info_text = str(e), title = "Error SRP", modal = True)

    @Slot()
    def retrieve(self, namesFiles = None):
        loadingPop = srpUi.LoadingForm(self, "Upload", "Sending files...")

        try:
            self.srpSystem.fileDownloaderSRP(lambda a, b : loadingPop.actionProgress(a, b), namesFiles) #Sending the files AND keeping track of the progress
            srpUi.PopupBox(info_text = "Files successfully retrieved!", icon = qtw.QMessageBox.Information, modal = False)
            self.mainWidget.locNav.reload(self.srpSystem.localCompFiles)
            self.mainWidget.remNav.reload(self.srpSystem.remoteCompFiles)

        except srp.exceptionSSH as e:
            loadingPop.close()
            srpUi.PopupBox(info_text = str(e), title = "Error SRP", modal = True)

    @Slot()
    def deleteFiles(self, namesFilesId, remote):
        try:
            self.srpSystem.removeFileSRP(namesFilesId, remote) #Sending the files AND keeping track of the progress
            srpUi.PopupBox(info_text = "Files successfully deleted!", icon = qtw.QMessageBox.Information, modal = False)
            self.mainWidget.locNav.reload(self.srpSystem.localCompFiles)
            self.mainWidget.remNav.reload(self.srpSystem.remoteCompFiles)

        except srp.exceptionSSH as e:
            srpUi.PopupBox(info_text = str(e), title = "Error SRP", modal = True)

    @Slot()
    def cfp(self, namesFilesId, remote):
        if remote:
            fileToCopy = self.srpSystem.remoteCompFiles.find(namesFilesId)[-1]
            clip.copy(pth.join(pth.dirname(self.srpSystem.settings["Remote"]),fileToCopy["Path"], fileToCopy["Name"]))
        else:
            fileToCopy = self.srpSystem.localCompFiles.find(namesFilesId)[-1]
            clip.copy(pth.join(pth.dirname(self.srpSystem.settings["Local"]),fileToCopy["Path"], fileToCopy["Name"]))

    @Slot()
    def srpParam(self):
        par = srpUi.ConfigWindow()
        par.exec_()

    @Slot()
    def srpUninstall(self):
        par = srpUi.DeleteForm()
        par.exec_()

if __name__ == '__main__':
    # Create the Qt Application
    app = qtw.QApplication(sys.argv)
    stream = QFile(pth.join(srp.pwdBasic, "styleLux.qss"))
    stream.open(QIODevice.ReadOnly)
    app.setStyleSheet(QTextStream(stream).readAll())
    # Create and show the form
    #form = srpUi.browseWindow()
    #form.show()
    form = Lux()
    form.show()
    # Run the main Qt loop
    sys.exit(app.exec_())
