import sys, time
from PySide2 import QtWidgets as qtw
from PySide2.QtCore import Qt, Slot, QObject
from PySide2 import QtGui as qtg

import srplib2 as srp

ORIG_PATH = srp.os.getcwd()

class PopupBox(qtw.QMessageBox):
    """A popup class for all the messages"""
    def __init__(self, info_text, title = "SRP information", icon = qtw.QMessageBox.Warning, modal = False):
        """ - icon : Type of icon for the popup (specified on the qt site), should be QtWidget.QMessageBox"""
        super(PopupBox, self).__init__()
        self.setIcon(icon)
        self.setWindowTitle(title)
        self.setModal(modal)
        self.setText(info_text)
        self.exec()#self.outputLocal.values = [str(e)]

class DeleteForm(qtw.QDialog):
    def __init__(self):
        super(DeleteForm, self).__init__()
        self.setWindowTitle("Uninstalling the SRP")
        self.setStyleSheet("""QPushButton:hover#Approve
        {
            background-color : #a40e0e;
        }
        QPushButton:pressed#Approve
        {
            background-color : #750606;
        }
        QPushButton#Approve
        {
            background-color : #840a0a;
        }""")

        self.label = qtw.QLabel("Do you wish to uninstall the SRP?\n It will leave an archive with the SRP's local files")
        self.icon = qtw.QLabel()
        self.icon.setPixmap(qtg.QPixmap("Graphics/warning.png"))
        self.buttonApprove = qtw.QPushButton("Uninstall")
        self.buttonApprove.setToolTip("You don't want to do that.")
        self.buttonApprove.setObjectName("Approve")
        self.buttonApprove.clicked.connect(self.uninstall)
        self.buttonDeny = qtw.QPushButton("Cancel")
        self.buttonDeny.clicked.connect(self.exit)

        lay = qtw.QHBoxLayout()
        lay.addWidget(self.buttonApprove)
        lay.addWidget(self.buttonDeny)
        lay2 = qtw.QHBoxLayout()
        lay2.addWidget(self.icon)
        lay2.addWidget(self.label)
        layF = qtw.QVBoxLayout()
        layF.addLayout(lay2)
        layF.addLayout(lay)

        self.setLayout(layF)

    def uninstall(self):
        conf = srp.ServInteract.configuration()
        conf.deleteConfig()
        self.close()

    def exit(self):
        self.close()

class LoadingForm(qtw.QProgressDialog):
    def __init__(self, host = None, title = "Progress", labelText = "Test", cancelButtonText = "Cancel"):
        # QProgressDialog::QProgressDialog(const QString &labelText, const QString &cancelButtonText, int minimum, int maximum, QWidget *parent = nullptr, Qt::WindowFlags f = Qt::WindowFlags())
        super(LoadingForm, self).__init__(title, cancelButtonText, 0, 99) #En pourcentage pour ma part
            #Parametres graphiques
        self.setWindowFlags(Qt.WindowTitleHint | Qt.Dialog | Qt.WindowMaximizeButtonHint | Qt.CustomizeWindowHint)
        self.setWindowTitle(title)
        self.setWindowModality(Qt.WindowModal)
            #Paramètres interface

    def actionProgress(self, sent, filename):
        filename = str(filename).strip("'")
        self.setValue(sent)
        self.setLabelText(filename)

class IdentificationForm(qtw.QDialog):
    """This class creates the form that will be generated to get the credentials of the user
        - icon : a qt Icon for the window"""
    def __init__(self, parent = None, icon = None):
        super(IdentificationForm, self).__init__(parent)
        # Set windows properties
        self.setFixedSize(300, 150)
        self.setWindowTitle("Authentication")
        if icon is not None:
            self.setWindowIcon(icon)
        # Create widgets
        self.unText = qtw.QLabel("Username:")
        self.username = qtw.QLineEdit("")
        self.pwdText = qtw.QLabel("Password:")
        self.password = qtw.QLineEdit("")
        self.password.setEchoMode(self.password.Password)
        self.accept = qtw.QPushButton("Ok")
        # Create layout and add widgets
        layout = qtw.QVBoxLayout()
        layout.addWidget(self.unText)
        layout.addWidget(self.username)
        layout.addWidget(self.pwdText)
        layout.addWidget(self.password)
        layout.addWidget(self.accept)
        # Set dialog layout
        self.setLayout(layout)
        # Add button signal to greetings slot
        self.accept.clicked.connect(self.accepted)

    @Slot()
    def accepted(self):
        self.close()

class ConfigWindow(qtw.QDialog):
    def __init__(self, folderProg_name = None, parent = None):
        super(ConfigWindow, self).__init__(parent)
        self.setFixedSize(300, 250)
        self.setWindowTitle("SRP Settings")
        self.setWindowIcon(qtg.QIcon("graphics/logo-srp.png"))
        self.folderName = folderProg_name

        self.listWidgetsConf = [None for i in range(6)]
        self.listWidgetsConf[0]  = qtw.QLabel('<p style="color:red;">Local Root Directory : </p>')
        self.listWidgetsConf[1] = qtw.QPushButton("Browse Files")
        self.listWidgetsConf[1].setToolTip("The local directory where will be store the files/folders retrieved with the SRP")
        self.listWidgetsConf[1].clicked.connect(self.browse)
        #self.Help1     = qtw.QLabel(values=["Relative path: The SRP will directly create a full path accordingly (in your home by default)",
        #            "Full Path: The SRP will store it directly but will send back an error if it doesn't exist",
        #            "Either way, the given directory shall an existing directory"])
        self.listWidgetsConf[2]   = qtw.QLabel('<p style="color:red;"> Server Root Directory : </p>')
        self.listWidgetsConf[3] = qtw.QLineEdit()
        self.listWidgetsConf[3].setToolTip("The server directory where will be store the files/folders sent with the SRP")
        self.listWidgetsConf[4]   = qtw.QLabel('<p style = "color:red;"> Server IP/DynDNS address : </p>')
        self.listWidgetsConf[5] = qtw.QLineEdit()
        self.listWidgetsConf[5].setToolTip("The address of the server where your files will be stored")

        self.depthTitle    = qtw.QLabel('<p style = "color:red;"> Maximum Recursion Depth : </p>')
        self.depthInd     = qtw.QLineEdit()
        self.depthInd.setValidator(qtg.QIntValidator(0, 100, self))
        self.depthInd.setToolTip("How deep will navigate the SRP through the directory arborescence")

        self.layoutDepth = qtw.QHBoxLayout()
        self.layoutDepth.addWidget(self.depthTitle)
        self.layoutDepth.addWidget(self.depthInd)

        self.apply = qtw.QPushButton("Apply")
        self.apply.clicked.connect(self.applySets)


        self.layout = qtw.QVBoxLayout()

        for i in self.listWidgetsConf:
            self.layout.addWidget(i)
        self.layout.addLayout(self.layoutDepth)
        self.layout.addWidget(self.apply)
        self.setLayout(self.layout)

    def applySets(self):
        try:
            conf = srp.ServInteract.configuration()
            conf.setConf({"Address" : self.listWidgetsConf[5].text(), "Local" : self.listWidgetsConf[1].text(), "Remote" : self.listWidgetsConf[3].text(), "Recursion" : self.depthInd.text()})
            conf.writeConfiguration()
        except Exception as e:
            print(e)
            PopupBox(info_text = str(e), title = "Error SRP", modal = True)
        self.close()

    def browse(self):
        files = qtw.QFileDialog()
        files.setOption(qtw.QFileDialog.ShowDirsOnly, True)
        files.setFileMode(qtw.QFileDialog.DirectoryOnly)
        files.exec_()
        self.listWidgetsConf[1].setText(files.directory().absolutePath())

class transferWindows:
    def __init__(self, srp_username = None, srp_password = None):
        if srp_password is None or srp_username is None:
            cred = IdentificationForm(icon = qtg.QIcon("graphics/logo-srp.png"))
            cred.exec_()
            self.username = cred.username.text()
            self.password = cred.password.text()
            self.srp = srp.ServInteract(self.username, self.password)
        else:
            self.username = srp_username
            self.password = srp_password
            self.srp = srp.ServInteract(self.username, self.password)

    def get(self):
        loadingPop = LoadingForm("Download", "Synchronization : Retrieving files...")
        try:
            self.srp.fileDownloaderSRP(lambda a, b : loadingPop.actionProgress(a, b)) #Retrieving the files and keeping tack of the progress
        except Exception as e:
            print("\033[31m" + "Error : " + "\033[0m" + str(e))
        srp.os.chdir(ORIG_PATH)

    def send(self):
        loadingPop = LoadingForm("Upload", "Synchronization : Sending files...")
        try:
            self.srp.fileSaverSRP(lambda a, b : loadingPop.actionProgress(a, b)) #Sending the files and keeping track of the progress
        except Exception as e:
            print("\033[31m" + "Error : " + "\033[0m" + str(e))
        srp.os.chdir(ORIG_PATH)
