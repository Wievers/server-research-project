import os
import os.path as pth
import subprocess as sp
import anytree as at
import datetime as dt

def findParent2(path, nameEnd = "SRP"):
    """Function finding  the parent node of file recursively. Created by fabnem
        - path [str] : The path of the file
        - dicoNoeuds [dict] : The dictionnary of the Node objects representing the tree
        - dicoRaw [dict] : Dictionnary of the files as we get them (SRP standards)"""
    if nameEnd not in path:
        return
    res = list()
    while path != nameEnd:
        res.append(path)
        path = pth.dirname(path)
    return res

def findParent(path, dicoNoeuds, dicoRaw):
    """Function finding  the parent node of file recursively. Created by fabnem
        - path [str] : The path of the file
        - dicoNoeuds [dict] : The dictionnary of the Node objects representing the tree
        - dicoRaw [dict] : Dictionnary of the files as we get them (SRP standards)"""
    pathParent = pth.dirname(path)
    if pathParent in dicoNoeuds:
        return dicoNoeuds[pathParent]
    else:
        nodeParent = at.Node(dicoRaw[pathParent], findParent(pathParent, dicoNoeuds, dicoRaw))
        dicoNoeuds[pathParent] = nodeParent
        return nodeParent

def fileToList(filename):
    buffListFile = list()
    while filename != "SRP":
        buffListFile.append(filename)
        filename = pth.dirname(filename)
    return [i for i in reversed(buffListFile)]

class folderView:
    def __init__(self, filename = None, list_filename = None):
        """ This method creates a new folderview instance.
            - gen : A generator to create the folderview or a list that
                represents the folderview (Ex: [file1, (nameFold1, [file11, file12]), ...])
            - listComplete : A list of the filenames of the folderview (Ex : [file1, folder1/file11, folder1/file12, ...])
            - list_verif   : A list to verify the folder names of the folderView (HIGHLY recommended while using the
                                folderview, specially if you have empty folders.)"""
        self.idCount = 1 #The name of the files are not reliable ids, because we could have exact same files in multiple folders
        self.root = None
        self.partialDictFiles = {"SRP" : at.Node({"Name" : "SRP", "Path" : "", "Size" : -1, "Latest" : 0, "IsFile" : False, "Id" : 0})}

        if filename is not None and list_filename is None:
            self.convertToTree(filename)

        elif list_filename is not None and filename is None:
            self.listToTree(list_filename)

        else:
            raise Exception("FolderView error; not enough parameters")

    def add(self, dictNewFiles):
        """Method that adds a new file/folder to the folderview and place it in the tree
            - file [str]: Name of the file/folder
            - isFile [bool]: boolean to inform whether it's a file or not
            - pathFile [str]: Path of the file relative to the root of the tree
            - size [float]: Size in byte of the file
            - latest [float]: date in UNIX timestamp basis"""
        #The standard of the folderview is file = dict() and file["Name"] exists
        res = dict()
        for i in dictNewFiles:
            for j in self.partialDictFiles:
                if dictNewFiles[i] != self.partialDictFiles[j].name:
                    res[i] = dictNewFiles[i]

        self.listToTree(res)

    def remove(self, filesToRemove):
        for i in filesToRemove:
            self.partialDictFiles[pth.join(i["Path"], i["Name"])].parent = None
            del self.partialDictFiles[pth.join(i["Path"], i["Name"])]

    def find(self, listFileId = None, recursion = 10):
        """Method finding matching/all files/folders in the folderview:
            - isFile [bool] : The parameter that defines whether we are matching files (True) or folders (False)
            - listFile [list] [default = None]: The list of the files to be matched"""
        res = list()
        #findFunc = lambda node : node.name["IsFile"] == isFile and (node.name["Id"] in listFileId or listFileId is None)
        findFunc = lambda node : (node.name["Id"] in listFileId or listFileId is None)
        allItems = at.findall(self.root, filter_=findFunc)

        for fold in allItems:
            listNames = findParent2(pth.join(fold.name["Path"], fold.name["Name"]))
            findFold = lambda node : (pth.join(fold.name["Path"], fold.name["Name"]) in node.name["Path"] or pth.join(node.name["Path"], node.name["Name"]) in listNames)
            allFiles = at.findall(self.root, filter_=findFold, maxlevel=recursion)

            for i in allFiles:
                res.append(i.name)
            #res.append(fold.name)
        return res

    def getTree(self):
        """Method to get the folderview layer by layer"""
        for children in at.LevelOrderGroupIter(self.root):
            yield [node.name for node in children]

    def convertToTree(self, FName):
        """Method scanning a tree via a filename. It then creates a Folderview object
            - filename [str] : The name of the folder to scan"""
        filesName = dict()
        os.chdir(os.path.dirname(FName))
        folder_name = os.path.basename(FName)
        for (root,dirs,files) in os.walk(folder_name, topdown=True):
            for i in files:
                filesName[pth.join(root, i)] = {"Name" : i, "Path" : root, "Size" : os.stat(pth.join(root, i)).st_size, "Latest" : os.stat(pth.join(root, i)).st_mtime, "IsFile" : True}
            for i in dirs:
                filesName[pth.join(root, i)] = {"Name" : i, "Path" : root, "Size" : os.stat(pth.join(root, i)).st_size, "Latest" : os.stat(pth.join(root, i)).st_mtime, "IsFile" : False}

        self.listToTree(filesName)

    def listToTree(self, dictFiles, debug = False):
        """Method creating a foldeview based on a list of files (This method is made based on SRP's standards)
            - dictFiles [dict] : dictionnary of the files used to render a folderview"""
        dictResult = self.partialDictFiles

        for filePath, infosFile in dictFiles.items(): #pour récupérer les infos du fichier et le path en même temps
            self.idCount += 1
            infosFile["Id"] = self.idCount
            if dictFiles[filePath]["IsFile"] and filePath not in dictResult.keys():
                dictResult[filePath] = at.Node(infosFile, findParent(filePath, dictResult, dictFiles))

        self.root = dictResult["SRP"]
        self.partialDictFiles = dictResult

        if debug:
            print("\033[31m Arborescence Folderview : \033[0m")
            for pre, fill, node in at.RenderTree(self.root):
                print("%s%s" % (pre, node.name))
